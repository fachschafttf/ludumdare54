extends Node2D
var elapsed_time = 0
var flag = false
var displayed_time = 0
var collected_points = 0
var walls_broken = 0
signal new_game

func _ready():
	pass

func _process(delta):
	elapsed_time += delta
	$Background.modulate.a = elapsed_time / 4
	$Label.text = "You survived for " + str(displayed_time) + " seconds, collected " + str(collected_points) + " points and broke down " + str(walls_broken) + " walls!"
	$Score.text = "Your score: " + str(int(displayed_time) * 10 + collected_points * 20 + walls_broken * 200)
	if elapsed_time >= 2 and flag == false:
		flag = true
		var scene = load("res://scenes/GameOverButtons.tscn")
		var buttonScene = scene.instantiate()
		add_child(buttonScene)
		var buttonSceneRoot = get_node("GameOverButtons")
		buttonSceneRoot.new_game_startup.connect(_on_new_game_startup)

# additional signal is needed, since GameOverButtons is loaded some time after the GameOverMenu
func _on_new_game_startup():
	new_game.emit()
