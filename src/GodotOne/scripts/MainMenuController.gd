extends Node
var gameMusic
var audioClick
var audioDie

func _ready():
	audioClick = $ButtonClick
	audioDie = $AudioDie
	pass

func _process(delta):
	pass

func _on_start_game_pressed():
	$StartGame.queue_free()
	$QuitGame.queue_free()
	audioClick.play()
	startup()

func startup():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	var scene = load("res://scenes/GameScene.tscn")
	var gameScene = scene.instantiate()
	add_child(gameScene)
	var player = get_node("GameScene/player")
	player.hit_harmful.connect(_on_hit_harmful)
	gameMusic = get_node("GameScene/Music")
	gameMusic.play()

func _on_quit_game_pressed():
	audioClick.play()
	get_tree().quit()

func _on_hit_harmful(time, points, walls):
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	var scene = load("res://scenes/GameOverMenu.tscn")
	var gameOverScene = scene.instantiate()
	add_child(gameOverScene)
	var GOMenu = get_node("GameOverMenu")
	GOMenu.displayed_time = time
	GOMenu.collected_points = points
	GOMenu.walls_broken = walls
	GOMenu.new_game.connect(_on_new_game)
	gameMusic.stop()
	audioDie.play()
	$GameScene.queue_free()

func _on_new_game():
	startup()
	$GameOverMenu.queue_free()
