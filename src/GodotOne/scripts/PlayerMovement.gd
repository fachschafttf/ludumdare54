extends Node2D
var speed = 3.5
signal hit_harmful(time, points)
signal collectedTwo()
signal collectedTen()
var body
var collected_points = 0
var pointcounter = 0
var walls_broken = 0
var broken_texture
var enemy
var audioPoint
var audioBreak
var audiotransform

func _ready():
	body = $Body
	audioPoint = $AudioPoint
	audioBreak = $AudioBreak
	audiotransform = $AudioTransform
	broken_texture = load("res://ressources/sprites/obstacle_purple_broken.png")
	enemy = get_node("../Enemy")
	enemy.hit_player.connect(_on_hit_player)

func _process(delta):
	body.velocity = Vector2.ZERO
	if Input.is_action_pressed("UpActionPlayer1"):
		body.velocity.y -=  speed
	elif Input.is_action_pressed("DownActionPlayer1"):
		body.velocity.y += speed
	if Input.is_action_pressed("RightActionPlayer1"):
		body.velocity.x += speed
	elif Input.is_action_pressed("LeftActionPlayer1"):
		body.velocity.x -= speed
	body.velocity = body.velocity.normalized() * speed

	var collision_obj = body.move_and_collide(body.velocity)
	if collision_obj:
		body.velocity = body.velocity.slide(collision_obj.get_normal())
		#print("collision: ", collision_obj.get_collider().name)
		handle_collision(collision_obj.get_collider())
		body.global_position += body.velocity
	else:
		$Overlay.position -= body.velocity.normalized() * 5 * delta
		$OverlayBlue.position -= body.velocity.normalized() * 5 * delta
	body.move_and_slide()


func handle_collision(object):
	if object.is_in_group("obstacle_harmful"):
		hit_harmful.emit($Body/Camera/Label.text, collected_points, walls_broken)
	elif object.is_in_group("point"):
		collected_points += 1
		pointcounter += 1
		if collected_points % 2 == 0:
			collectedTwo.emit()
		if pointcounter >= 10:
			pointcounter = 0
			collectedTen.emit()
			audiotransform.play()
		object.get_parent().queue_free()
		audioPoint.play()
	elif object.is_in_group("breakable"):
		object.get_parent().get_child(0).texture = broken_texture
		object.queue_free()
		audioBreak.play()
		walls_broken += 1

func _on_hit_player():
	hit_harmful.emit($Body/Camera/Label.text, collected_points, walls_broken)
