extends Node2D
var rng = RandomNumberGenerator.new()
var texture_red = load("res://ressources/sprites/obstacle_red.png")
var texture_purple = load("res://ressources/sprites/obstacle_purple.png")
var player
var map_center
var scene

# Called when the node enters the scene tree for the first time.
func _ready():
	player = $player/Body
	scene = load("res://scenes/point.tscn")
	player.get_parent().collectedTen.connect(_on_collectedTen)
	for obstacle in $obstacle_parkour.get_children():
		var random = rng.randi() % 10
		if random < 2:
			obstacle.get_child(0).queue_free()
			obstacle.get_child(1).queue_free()
		elif random < 4:
			var sprite = obstacle.get_child(0)
			sprite.texture = texture_red
			obstacle.get_child(1).add_to_group("obstacle_harmful")
		elif random < 5:
			var sprite = obstacle.get_child(0)
			sprite.texture = texture_purple
			obstacle.get_child(1).add_to_group("breakable")

	map_center = player.global_position
	player.get_parent().collectedTwo.connect(_on_collectedTwo)
	for i in range(100):
		place_point()

func _process(delta):
	pass

func _on_collectedTwo():
	place_point()

func _on_collectedTen():
	var flag = true
	var allWalls = $obstacle_parkour.get_children()
	while flag:
		var obj = rng.randi() % allWalls.size()
		var collider = $obstacle_parkour.get_child(obj).get_child(1)
		# crunch solution
		if collider != null:
			if not collider.is_in_group("breakable"):
				collider.add_to_group("breakable")
				collider.get_parent().get_child(0).texture = texture_purple
				collider.remove_from_group("obstacle_harmful")
				flag = false

func place_point():
	var gameScene = scene.instantiate()
	var posX = rng.randi() % 2000
	var posY = rng.randi() % 2000
	var leng = rng.randi() % 650
	if rng.randi() % 2 < 1:
		posX = -posX
	if rng.randi() % 2 < 1:
		posY = -posY
	var pos = Vector2(posX, posY).normalized() * (leng + 100) + map_center
	gameScene.global_position = pos
	$Points.add_child(gameScene)
