extends Node2D
var player
var target = Vector2.ZERO
var body
var speed = 3
signal hit_player

func _ready():
	player = get_node("../player/Body")
	body = $Body


func _process(delta):
	var vec_to_player = body.global_position - player.global_position
	var collision_obj = body.move_and_collide(-vec_to_player.normalized() * speed)
	if collision_obj:
		body.velocity = body.velocity.slide(collision_obj.get_normal())
		body.global_position += body.velocity
		if collision_obj.get_collider().is_in_group("player"):
			hit_player.emit()
	body.move_and_slide()
