extends Node2D

var min_scale = Vector2(0.1, 0.1)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	var shrink = Vector2(0.0002, 0.0002)
	var new_size = scale - shrink
	if new_size.x > min_scale.x:
		scale = new_size
		for child in get_children():
			child.scale.x += (-new_size.x + 1) * shrink.x * 75
