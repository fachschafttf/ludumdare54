extends Node2D
var elapsed_time = 0
var audioClick
signal new_game_startup

func _ready():
	$TryAgain.modulate.a = elapsed_time
	$QuitGame.modulate.a = elapsed_time
	audioClick = $ButtonClick

func _process(delta):
	elapsed_time += delta
	$TryAgain.modulate.a = elapsed_time
	$QuitGame.modulate.a = elapsed_time


func _on_try_again_pressed():
	audioClick.play()
	new_game_startup.emit()

func _on_quit_game_pressed():
	audioClick.play()
	get_tree().quit()
