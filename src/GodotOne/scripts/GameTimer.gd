extends Label
var elapsed_time = 0
var player

# Called when the node enters the scene tree for the first time.
func _ready():
	text = ""


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	elapsed_time += delta
	var index = str(elapsed_time).find(".")
	text = str(elapsed_time).substr(0, index + 3)
	get_node("../../../Overlay").modulate.a = sin(elapsed_time / 1.5)
	get_node("../../../OverlayBlue").modulate.a = -sin(elapsed_time / 1.5)
