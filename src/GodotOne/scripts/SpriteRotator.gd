# slightly changed from godot tutorial
# https://docs.godotengine.org/en/stable/getting_started/step_by_step/scripting_player_input.html
extends Sprite2D

var axis = Vector2(1,0)

func _process(delta):
	var input_direction = Input.get_vector("LeftActionPlayer1", "RightActionPlayer1", "UpActionPlayer1", "DownActionPlayer1")
	if input_direction != Vector2.ZERO:
		if Input.is_action_pressed("UpActionPlayer1") and Input.is_action_pressed("LeftActionPlayer1"):
			rotation_degrees = 225
		elif Input.is_action_pressed("UpActionPlayer1") and Input.is_action_pressed("RightActionPlayer1"):
			rotation_degrees = 315
		elif Input.is_action_pressed("UpActionPlayer1"):
			rotation_degrees = 270
		else :
			rotation = acos((input_direction.dot(axis)) / (input_direction.length() * axis.length()))
