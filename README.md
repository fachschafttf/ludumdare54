# Ludum Dare 54

This is our game for the 54th [LDJam](https://ldjam.com)

## Repo Setup
Godot 4.1.1


Enable large file system!

```
git lfs install
```

## Play the game

Latest Build: https://fachschafttf.gitlab.io/ludumdare54/
